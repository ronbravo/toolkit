Package.describe({
  name: 'thetrg:toolkit',
  version: '0.1.0',
  // Brief, one-line summary of the package.
  summary: 'The browser frontend Ui Toolkit in the Meteor and Gibson frameworks.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://github.com/ronbravo/toolkit',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.7.0.5');
  api.use('ecmascript');
  api.mainModule('toolkit.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('thetrg:toolkit');
  api.mainModule('toolkit-tests.js');
});
