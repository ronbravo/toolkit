// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by toolkit.js.
import { name as packageName } from "meteor/thetrg:toolkit";

// Write your tests here!
// Here is an example.
Tinytest.add('toolkit - example', function (test) {
  test.equal(packageName, "toolkit");
});
