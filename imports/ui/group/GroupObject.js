import { App, BaseObject } from 'meteor/thetrg:gibson';

import GroupData from './GroupData.js';
import GroupUi from './GroupUi.js';

export default class GroupObject extends BaseObject {
  constructor(config = {}) {
    super(App.merge(config, {
      dataType: GroupData,
      uiType: GroupUi,
    }));
  }
}
