import { App, DomUi } from 'meteor/thetrg:gibson';

import { template } from './Group.hbs';

export default class GroupUi extends DomUi {
  constructor(config) {
    super(App.merge(config, {
      graphic: template,
    }));
  }
}
