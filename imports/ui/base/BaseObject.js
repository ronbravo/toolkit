import App from '../App.js';
import Base from './Base.js';

/**
 * class: BaseObject
 * description: ...
 */
export default class BaseObject extends Base {
  constructor(config = {}) {
    let type;

    super(App.merge({
      _setObject: true,
    }, config));

    if (config.dataType) {
      this.setData(new config.dataType(config));
    }

    if (config.uiType) {
      this.setUi(new config.uiType(config));
    }

    // Auto load data.
    if (config.load) {
      load = config.load;

      // Check if result set has been provided.
      if (load.result) {
        this.getHierarchy().addChildrenFromDataList(load.type, load.result, true);
      }
    }
  }

  addChildren(type, list) {
    let hierarchy;
    hierarchy = this.getHierarchy();

    if (!list) {
      list = type;
      hierarchy.addChildren(list);
    }
    else {
      hierarchy.addChildrenFromDataList(type, list);
    }
  }

  clearChildren() {
    let ui;
    ui = this.getUi();
    ui.clearChildren();
  }

  getData(raw) {
    if (raw && this.data) {
      return this.data.getRaw();
    }
    return this.data;
  }

  getHierarchy() {
    return this.hierarchy;
  }

  getUi() {
    return this.ui;
  }

  getInfo() {
    if (this.data) { this.data._info = App.updateInfo(); }
    if (this.data) { return this.data._info; }
  }

  setData(data) {
    if (data) {
      this.data = data;
      data.setObject(this);
    }
  }

  setRawData(data) {
    if (data) { this.data.setData(data); }
  }

  setUi(ui) {
    if (ui) {
      this.ui = ui;
      ui.setObject(this);
    }
  }

  setup() {
    // if (this.data) { this.data.setup(); }
    if (this.ui) { this.ui.setup(); }
    return this;
  }
}
