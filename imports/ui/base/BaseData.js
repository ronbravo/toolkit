import App from '../App.js';
import Base from './Base.js';

export default class BaseData extends Base {
  constructor(config = {}) {
    super(config);
    if (!config.data) { config.data = {}; }
    if (!config.data._info) { config.data._info = App.updateInfo({}); }

    this.data = config.data;

    if (config.object) {
      this.object = config.object;
      this.data._info.type = this.object.constructor.name;
      this.data._info.temp = {
        object: this.object,
      }
    }
  }

  addChildrenFromList(type, list) {
    const hierarchy = this.getObject().getHierarchy();
    let end, i, item, object;

    end = list.length;
    for (i = 0; i < end; i++) {
      item = list[i];
      object = new type({ data: item });

      object.setRawData(item);
      hierarchy.addChild(object);
    };
  }

  getObject() {
    return this.object;
  }

  getRaw() {
    return this.data;
  }

  // getUi() {
  //   return this.object.getUi();
  // }

  load(args) {
    let result = [];
    if (args.result) {
      result = args.result;
    }
    else {
      // Run query;
      // const result = [];
    }

    this.addChildrenFromList(args.type, result);
    return result;
  }

  loadAppInstance(graphic) {
    let end, i, item, list;
    const query = this.appQuery;

    // TODO: Run query...

    // NOTE: Testing data.
    result = {
      fetch: function(){
        return [
          {},
        ];
      },
    };

    // Add the app graphic to each of the results.
    list = result.fetch();
    end = list.length;
    for (i = 0; i < end; i++) {
      item = list[i];
      item.ui = graphic;

      // Assign defaults.
      if (!item.name) { item.name = this.data.name; }
    }

    return list;
  }

  setObject(object) {
    this.object = object;
    // console.log('constructor: ', this.object.constructor);
    this.data._info.type = this.object.constructor.name;
    // console.log('data:', this.data);
  }

  setData(data) {
    if (!data._info) {
      data._info = {};
      App.updateInfo(data._info);
    }
    App.setMethods(this.object, data._info);

    this.data = data;
  }
}
