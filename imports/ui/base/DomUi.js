import App from '../App.js';
import BaseUi from './BaseUi.js';

import Handlebars from 'handlebars';
import { noTemplate } from './BaseHierarchy.html';

Handlebars.registerHelper('bind', function(name, args) {
  let data, hash;

  // Grab the info from the helper.
  hash = args.hash;
  data = args.data.root;

  if (data) {
    if (!data._bindList) { data._bindList = []; }
    data._bindList.push(name);
  }

  // Send the new bound data.
  return new Handlebars.SafeString('<span data-bound-object-id="' + data._info.id + '" data-prop="' + name + '"></span>');
});

Handlebars.registerHelper('bindAttr', function() {
  let data, hash;

  // Grab the info from the helper.
  hash = args.hash;
  data = args.data.root;

  // Send the new bound data.
  return '';
});
Handlebars.registerHelper('run', function() {
  let argList, args, data, hash, name;

  // Grab the info from the helper.
  argList = [].slice.call(arguments);

  if (argList.length > 1) {
    name = argList[0];
    args = argList[argList.length - 1];

    hash = args.hash;
    data = args.data.root;

    if (data && data._info && data._info.temp) {
      ui = data._info.temp.object.getUi();
      if (ui[name]) {
        return ui[name](args.hash);
      }
    }
  }

  // Send the new bound data.
  return '';
});
Handlebars.registerHelper('children', function(args) {
  let childHtml, html, object, ui;

  // Grab the info from the helper.
  html = '';
  data = args.data.root;

  if (data && data._info && data._info.temp) {
    object = data._info.temp.object;
    ui = object.getUi();
    html = ui.renderChildrenHtml(data);
  }

  return html;
});
Handlebars.registerHelper('prop', function() { return ''; });
Handlebars.registerHelper('child', function() { return ''; });

/**
* class: DomUi
* description: The ui layer for the browser side of the visuals for an object.
*/
export default class DomUi extends BaseUi {
  constructor(config = {}) {
    super(config);

    // Bind is how to bind ui events to object methods.
    if (!config.graphic) { this.setGraphic(noTemplate); }
    this.eventConfig = {};
    this.helperConfig = {};
    this.dom = {
      main: null,
      binded: {},
    }
  }

  bindData() {
    let data, domList, end, i, item, key, list, mainDom;

    mainDom = this.dom.main;
    if (mainDom) {
      data = this.getObject().getData(true);
      domList = mainDom.querySelectorAll('[data-bound-object-id="' + data._info.id + '"]');
      // console.log('*** here:', data._info.id, domList);

      if (domList.length) {
        domList = [].slice.call(domList);

        list = domList;
        end = list.length;
        for (i = 0; i < end; i++) {
          item = list[i];

          if (!item.isBound) {
            item.isBound = true;
            key = item.dataset.prop;

            // Check if the found dom has a prop to be bound.
            if (key) {
              item.textContent = data[key];
              this.dom.binded[key] = item;
            }
          }
        }
      }
    }
  }

  bindEvent() {
    let callback, dom, evntList, item, key, list, mainDom;

    mainDom = this.dom.main;
    if (mainDom) {
      function wrap(context, method) {
        return function() {
          method.apply(context, [].slice.call(arguments));
        }
      }

      list = this.eventConfig;
      for (key in list) {
        item = list[key];

        // Break the item listener up.
        if (!item.evnt) {
          evntList = key.split(',');
          evnt = evntList[0];
          selector = evntList[1].trim();

          item = {
            evnt: evnt,
            selector: selector,
            method: item,
          }

          dom = mainDom.querySelector(selector);
          if (dom && item.method) {
            dom.addEventListener(item.evnt, wrap(this, item.method));
          }
          list[key] = item;
        }
      }
    }
  }

  bindUi() {
    let data, dom, end, i, item, list, selector;

    data = this.getObject().getData(true);
    if (data) {
      selector = "[id='" + data._info.id + "']";
      dom = document.querySelector(selector);

      if (dom) {
        dom.object = this.object;
        this.dom.main = dom;
      }

      // TODO: Make sure binding does not happen multiple times.
      this.bindData();
      this.bindEvent();
    }

    // Bind the children.
    list = this.object.getHierarchy().getChildList();
    end = list.length;
    for (i = 0; i < end; i++) {
      item = list[i];
      item.getUi().bindUi();
    }

    return this;
  }

  clearChildren() {
    let childArea, mainDom;

    mainDom = this.dom.main;
    if (mainDom) {
      data = this.object.getData(true);
      childArea = mainDom.querySelector('[data-parent-id="' + data._info.id + '"][data-children]');

      // Clear out the child dom elements.
      if (childArea) {
        childArea.innerHTML = '';
      }
    }

    // TODO: Clear the listeners and the binding for the elements.
  }

  getMainDom() {
    return this.dom.main;
  }

  render() {
    let childHtml, childList, data, end, graphic, html, i, item, list;

    html = '';
    childHtml = '';
    data = this.getObject().getData(true);
    graphic = this.graphic;

    // Render this object.
    if (data && graphic) {
      // Render this object.
      html = graphic(data);
      delete data._childHtml;
    }

    return html;
  }

  renderData(newData) {
    let dom, item, key, list;

    list = newData;
    for (key in list) {
      item = list[key];
      dom = this.dom.binded[key];

      if (dom) {
        dom.textContent = item;
      }
    }
  }

  renderChild() {}

  renderChildrenHtml() {
    let end, htmlList, i, item, list;

    htmlList = [
      '<ul class="render children" data-parent-id="' + data._info.id + '" data-children>',
    ];

    // Render the children.
    list = this.object.getHierarchy().getChildList();
    end = list.length;
    for (i = 0; i < end; i++) {
      item = list[i];
      htmlList.push(this.wrapChildHtml(item.getUi().render(), item.getData(true)));
    }

    htmlList.push('</ul>');
    html = htmlList.join('\n');

    return html;
  }

  renderChildrenDom(newChildren, childArea) {
    let child, data, dom, end, html, i, item, list, mainDom, ui;

    html = '';
    childHtml = '';

    list = this.object.getHierarchy().getChildList();
    if (newChildren) { list = newChildren; }

    mainDom = this.dom.main;
    if (mainDom) {
      if (!childArea) {
        data = this.object.getData(true);
        childArea = mainDom.querySelector('[data-parent-id="' + data._info.id + '"][data-children]');
      }

      if (childArea) {
        dom = document.createElement('div');

        // Render the child elements to the page.
        if (list.length) {
          end = list.length;
          for (i = 0; i < end; i++) {
            item = list[i];
            ui = item.getUi();
            data = item.getData(true);
            html = ui.render();

            // Check if we are to add to the child area or the dom.
            dom.innerHTML = this.wrapChildHtml(html, data);
            child = dom.children[0];
            childArea.appendChild(child);
          }

          // Update the links and bind the ui props.
          this.bindUi();
          App.updateLinks();
        }
      }
    }
    else {
      console.log('no main dom found for:', this.constructor.name, this.object.getData(true)._info.id);
    }
  }

  renderProp() {}

  uiEvent(config) {
    let item, key, list;

    list = config;
    for (key in list) {
      item = list[key];
      this.eventConfig[key] = item;
    }
  }

  uiHelper(config) {
    let item, key, list;

    list = config;
    for (key in list) {
      item = list[key];
      this.helperConfig[key] = item;
    }
  }

  updateData(newData, doNotRenderUpdate) {
    let data, end, i, item, list;

    // Update the data changes.
    data = this.object.getData(true);
    list = newData;
    for (key in list) {
      item = list[key];
      data[key] = item;
    }

    // Render the changes to the Dom.
    if (!doNotRenderUpdate) {
      this.renderData(newData);
    }
  }

  wrapChildHtml(html, data) {
    return '<li data-child-id="' + data._info.id + '" class="item" data-child>' + html + '</li>';
  }
}
