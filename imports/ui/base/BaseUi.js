import { Meteor, Moment, Navigo, ReactiveDict, ReactiveVar, Template, Voca } from '../Dependency.js';
import App from '../App.js';
import Base from './Base.js';

/**
 * class: BaseUi
 * description: This is the base class for an object's visual presentation.
 */
export default class BaseUi extends Base {
  constructor(config = {}) {
    super(config);

    // Bind is how to bind ui events to
    // object methods.
    this.object = config.object;
    this.setGraphic(config.graphic);
  }

  clearChildren() {
  }

  getObject() {
    return this.object;
  }

  render() {
  }

  setup() {
    return this;
  }

  setGraphic (graphic) {
    if (graphic) {
      this.graphic = graphic;
    }
  }

  setObject(object) {
    this.object = object;
  }

  update(newData) {
  }
}
