'use strict';
import { Moment, ReactiveDict, ReactiveVar } from '../Dependency.js';
import App from '../App.js';

export default class BaseHierarchy {
  constructor(config) {
    this.object = config.object;
    this.children = {
      alias: {},
      list: [],
    }
  }

  addChild(key, child) {
    if (!child) {
      child = key;
    }
    else {
      this.children.alias[key] = child;
    }

    this.children.list.push(child);
  }

  addChildren(list, doNotRender) {
    let end, i, item, newChildren, obj, object, ui;

    object = this.object;
    if (object) {
      newChildren = [];
      ui = object.getUi();

      end = list.length;
      for (i = 0; i < end; i++) {
        this.children.list.push(obj);
        newChildren.push(obj);
      }

      if (newChildren.length && ui) {
        if (!doNotRender) {
          ui.renderChildrenDom(newChildren);
        }
      }
    }
  }

  addChildrenFromDataList(type, list, doNotRender) {
    let end, i, item, newChildren, obj, object, ui;

    object = this.object;
    if (object) {
      newChildren = [];
      ui = object.getUi();
      let id = object.getData(true)._info.id;


      end = list.length;
      for (i = 0; i < end; i++) {
        item = list[i];
        obj = new type({ data: item });

        this.children.list.push(obj);
        newChildren.push(obj);
      }

      if (newChildren.length && ui) {
        if (!doNotRender) {
          ui.renderChildrenDom(newChildren);
        }
      }
    }
  }

  clearChildren() {
    let ui = object.getUi();
    this.children.alias = {};
    this.children.list = [];
    ui.clearChildren();
  }

  getChildren() {
    return this.children;
  }

  getChild(id) {
    if (App.getType(id) === 'String') {
      return this.children.alias[id];
    }
    return this.children.list[id];
  }

  getChildList() {
    return this.children.list;
  }

  setChild(key, child) {
    let index, oldChild;


    // TODO: Loop through and destroy() each of the objects.
    oldChild = this.children.alias[key];
    index = this.children.list.indexOf(oldChild);

    if (index !== -1) {
      this.children.alias[key] = child;
      this.children.list[index] = child;
    }
  }
}
