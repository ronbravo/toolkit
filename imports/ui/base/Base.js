import App from '../App.js';
import BaseHierarchy from './BaseHierarchy';

export default class Base {
  constructor(config) {
    // className
    if (config._setObject) {
      config.object = this;
      delete config._setObject;
    }

    this.hierarchy = new BaseHierarchy(config);
  }

  getAllMethods(obj) {
    // ref: https://stackoverflow.com/a/31055217/3268255
    var props = [];
    do {
      props = props.concat(Object.getOwnPropertyNames(obj));
    } while (obj = Object.getPrototypeOf(obj));

    if (obj) {
      return props.sort().filter(function(e, i, arr) {
        if (e!=arr[i+1] && typeof obj[e] == 'function') return true;
      });
    }

    return props;
  }

  merge(source, target) {
    return App.merge(source, target);
  }
}
