import { App, BaseObject } from 'meteor/thetrg:gibson';

import MenuData from './MenuData.js';
import MenuUi from './MenuUi.js';

export default class MenuObject extends BaseObject {
  constructor(config = {}) {
    super(App.merge(config, {
      dataType: MenuData,
      uiType: MenuUi,
    }));
  }
}
