import { App, DomUi } from 'meteor/thetrg:gibson';

import { template } from './Menu.hbs';

export default class MenuUi extends DomUi {
  constructor(config) {
    super(App.merge(config, {
      graphic: template,
    }));
  }
}
